#!/usr/bin/env bash

# Example:
# get_certicate certname domain.com,www.domain.com

sudo certbot --apache --cert-name $1 -d $2
