#!/usr/bin/env bash

# Example:
# revoke_certicate certname

sudo certbot revoke --cert-path /etc/letsencrypt/live/$1/cert.pem
