#!/usr/bin/env bash

# Example:
# assign_user [username database_name]

if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi

read -p "Username [$1]: " USER
USER=${USER:-$1}

read -p "Database name [$2]: " MAINDB
MAINDB=${MAINDB:-$2}

mysql -e "GRANT ALL PRIVILEGES ON ${MAINDB}.* TO '${USER}'@'localhost';"
mysql -e "FLUSH PRIVILEGES;"
