#!/usr/bin/env bash

# Example:
# duplicate_db db_name_original new_db_name

if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi

read -p "Original database name [$1]: " DBORIGIN
DBORIGIN=${DBORIGIN:-$1}

read -p "New database name [$2]: " DBDESTINY
DBDESTINY=${DBDESTINY:-$2}

#mysql -u root -e "DROP DATABASE $DBDESTINY;"
mysql -u root -e "CREATE DATABASE $DBDESTINY;"
mysqldump -u root $DBORIGIN | mysql -u root $DBDESTINY
