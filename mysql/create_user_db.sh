#!/usr/bin/env bash

# Example:
# create_user [username database_name]

if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi

read -p "User [$1]: " input
USER=${input:-$1}

read -p "Password: " input
PASS=${input}

read -p "Database name [$2]: " input
MAINDB=${input:-$2}

mysql -e "CREATE DATABASE ${MAINDB} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
mysql -e "CREATE USER ${USER}@localhost IDENTIFIED BY '${PASS}';"
mysql -e "GRANT ALL PRIVILEGES ON ${MAINDB}.* TO '${USER}'@'localhost';"
mysql -e "FLUSH PRIVILEGES;"
mysql -e "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '${MAINDB}'"
